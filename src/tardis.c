#include "tardis.h"

#include <errno.h>
#include <fcntl.h>
#include <math.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <pcap.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

/* BEGIN: Internal functions and data declarations */
/* Common data structures */
void tardis_spawn_thread(pthread_t *thread,
                         pthread_attr_t *attr,
                         void * (*routine) (void *),
                         void *data,
                         char *name);
/*
Acquisition section

All functions, structures, and data here relate to the capture of NTP data
from the target network.  This all occurs in a single pthread and is passed
off to other threads via shared memory references.
*/
struct tardis_internal_data {
    char *interface;
    int refclock_val;
    struct tardis_time_acq_t *acq_buffer;
    sem_t *acq_sem_in;
    sem_t *acq_sem_out;
    sem_t *offset_sem;
    double *offset;
};
void *tardis_acquisition_thread(void *internal_data);
struct tardis_time_acq_t tardis_process_packet (
                         const struct pcap_pkthdr *header,
                         const u_char *packet);

/*
Mathematics section

Functions, structures, and data for building a mathematical model that
smoothly 'adjusts' the local clock to what is observed on the network.
*/
void *tardis_mathematics_thread(void *internal_data);
struct tardis_time_acq_list {
    struct tardis_time_acq_list *prev;
    struct tardis_time_acq_list *next;
    struct tardis_time_acq_t val;
};

struct tardis_host_time_t {
    struct in_addr host;
    int stratum;
    double offset;
    double last_polled;
    double poll_interval;
};

struct tardis_host_time_list {
    struct tardis_host_time_list *next;
    struct tardis_host_time_list *prev;
    struct tardis_host_time_t val;
};
/*
Redistribution section

Functions, structures, and data for serving as an NTP reference clock using
the mathematical model calculated in the 'Mathematics' section
*/
void *tardis_redistribution_thread(void *internal_data);

/* END: Internal functions and data declarations */

int tardis_launch(char* interface, int refclock_val)
{
    pthread_t acquire;
    pthread_t calculate;
    pthread_t redistribute;
    pthread_attr_t attr;
    struct tardis_internal_data int_data;
    void *status;
    int rc;

    sem_t *acq_sem_in;
    acq_sem_in = malloc(sizeof(sem_t));

    sem_t *acq_sem_out;
    acq_sem_out = malloc(sizeof(sem_t));

    sem_t *offset_sem;
    offset_sem = malloc(sizeof(sem_t));

    if (sem_init(acq_sem_in, 0, TARDIS_ACQ_BUFF_SIZE))
    {
        fputs("ERROR: Failed to create 'acq_sem_in'\n", stderr);
        exit(-1);
    }

    if (sem_init(acq_sem_out, 0, 0))
    {
        fputs("ERROR: Failed to create 'acq_sem_out'\n", stderr);
        exit(-1);
    }

    if (sem_init(offset_sem, 0, 1))
    {
        fputs("ERROR: Failed to create 'offset_sem'\n", stderr);
        exit(-1);
    }

    double offset;
    offset = 0.0;


    struct tardis_time_acq_t *acq_buffer;

    acq_buffer = calloc(TARDIS_ACQ_BUFF_SIZE, sizeof(struct tardis_time_acq_t));

    int_data.interface = interface;
    int_data.refclock_val = refclock_val;
    int_data.acq_buffer = acq_buffer;
    int_data.acq_sem_in = acq_sem_in;
    int_data.acq_sem_out = acq_sem_out;
    int_data.offset_sem = offset_sem;
    int_data.offset = &offset;

    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

    tardis_spawn_thread(&acquire,
                        &attr,
                        tardis_acquisition_thread,
                        (void *) &int_data,
                        "acquisition");

    tardis_spawn_thread(&calculate,
                        &attr,
                        tardis_mathematics_thread,
                        (void *) &int_data,
                        "mathematics");

    tardis_spawn_thread(&redistribute,
                        &attr,
                        tardis_redistribution_thread,
                        (void *) &int_data,
                        "redistribution");

    pthread_attr_destroy(&attr);

    rc = pthread_join(acquire, &status);

    sem_destroy(acq_sem_in);
    free(acq_sem_in);

    sem_destroy(acq_sem_out);
    free(acq_sem_out);

    sem_destroy(offset_sem);
    free(offset_sem);

    //free(offset);
    free(acq_buffer);

    return(0);
}


void tardis_spawn_thread(pthread_t *thread,
                         pthread_attr_t *attr,
                         void * (*routine) (void *),
                         void *data,
                         char *name)
{
    int rc;
    rc = pthread_create(thread, attr, routine, data);

    if (rc)
    {
        fprintf(stderr, "Error starting %s thread: %d\n", name, rc);
        exit(-1);
    }
}

void *tardis_acquisition_thread(void *internal_data)
{
    struct tardis_internal_data *int_data;
    char error_buffer[PCAP_ERRBUF_SIZE];
    struct pcap_pkthdr packet_header;
    struct bpf_program bpf_filter;
    pcap_t *handle;

    int_data = (struct tardis_internal_data *) internal_data;

    handle = pcap_open_live(
        int_data->interface,
        TARDIS_PCAP_SNAPLEN,
        TARDIS_PCAP_PROMISC,
        TARDIS_PCAP_TIMEOUT,
        error_buffer);

    if (handle == NULL)
    {
        fprintf(stderr, "ERROR: %s\n", error_buffer);
        exit(1);
    }

    pcap_compile(handle,
                 &bpf_filter,
                 TARDIS_BPF_FILTER,
                 TARDIS_BPF_OPTIMIZE,
                 PCAP_NETMASK_UNKNOWN);

    pcap_setfilter(handle, &bpf_filter);

    const u_char *packet;

    sem_t *acq_sem_in;
    sem_t *acq_sem_out;
    struct tardis_time_acq_t *acq_buffer;
    acq_buffer  = int_data->acq_buffer;
    acq_sem_in  = int_data->acq_sem_in;
    acq_sem_out = int_data->acq_sem_out;

    uint32_t acq_buff_index = 0;

    while (1)
    {
        packet = pcap_next(handle, &packet_header);

        if (packet != NULL)
        {
            sem_wait(acq_sem_in);

            acq_buffer[acq_buff_index] = tardis_process_packet(&packet_header, packet);
            printf("ACQUISITION: Enqueued %2d with ts=%f\n", acq_buff_index, acq_buffer[acq_buff_index].cap_ts);
            acq_buff_index = (1 + acq_buff_index) % TARDIS_ACQ_BUFF_SIZE;

            sem_post(acq_sem_out);
        }
    }
}

struct tardis_time_acq_t tardis_process_packet (
    const struct pcap_pkthdr *header,
    const u_char *packet)
{
    struct tardis_time_acq_t time_acq;
    time_acq.cap_ts = 0.0;

    if (header->caplen < header->len)
    {
        fprintf(stderr, "ERROR: Only captured %d of %d bytes!",
                header->caplen, header->len);
        return time_acq;
    }

    uint8_t ethernet_header_length; // Constant

    if (*(packet+12) == 0x81 && *(packet+13) == 0)
        ethernet_header_length = 18; // 802.1Q
    else
        ethernet_header_length = 14;

    struct ip *ip_header;
    uint8_t ip_header_length;

    struct udphdr *udp_header;
    uint8_t udp_header_length = 8; // Constant
    uint16_t udp_datagram_length;

    struct tardis_ntp_t *ntp_data;
    uint16_t payload_length;

    ip_header = (struct ip *)(packet + ethernet_header_length);
    ip_header_length = (ip_header->ip_hl) << 2;

    udp_header = (struct udphdr *)(packet
                                   + ethernet_header_length
                                   + ip_header_length);
    udp_datagram_length = ntohs(udp_header->uh_ulen);

    payload_length = udp_datagram_length - udp_header_length;

    if (payload_length < 48)
    {
        fputs("ERROR: Packet too short for NTP!", stderr);
        return time_acq;
    }
    else if (payload_length % 8 != 0)
    {
        fputs("ERROR: Packet not padded to 64-bit boundary!", stderr);
        return time_acq;
    }
    else if (payload_length > 48)
    {
        fputs("WARNING: NTPv4 extensions may be present.", stderr);
    }


    ntp_data = (struct tardis_ntp_t *) (packet
                                        + ethernet_header_length
                                        + ip_header_length
                                        + udp_header_length);

    time_acq.cap_ts = header->ts.tv_sec + (header->ts.tv_usec/1000000.0);
    time_acq.src_ip = ip_header->ip_src;
    time_acq.dst_ip = ip_header->ip_dst;
    time_acq.src_port = ntohs(udp_header->uh_sport);
    time_acq.dst_port = ntohs(udp_header->uh_dport);
    time_acq.stratum = ntp_data->stratum;
    time_acq.ref_id = ntp_data->referenceID;

    time_acq.ref_ts = ntohl(ntp_data->ref_ts_sec)
            + (ntohl(ntp_data->ref_ts_frac) * TARDIS_NTP_FRACT_SECS)
            - TARDIS_NTP_UNIX_OFFSET;

    time_acq.orig_ts = ntohl(ntp_data->origin_ts_sec)
            + (ntohl(ntp_data->origin_ts_frac) * TARDIS_NTP_FRACT_SECS)
            - TARDIS_NTP_UNIX_OFFSET;

    time_acq.recv_ts = ntohl(ntp_data->recv_ts_sec)
            + (ntohl(ntp_data->recv_ts_frac) * TARDIS_NTP_FRACT_SECS)
            - TARDIS_NTP_UNIX_OFFSET;

    time_acq.trans_ts = ntohl(ntp_data->trans_ts_sec)
            + (ntohl(ntp_data->trans_ts_frac) * TARDIS_NTP_FRACT_SECS)
            - TARDIS_NTP_UNIX_OFFSET;

    return time_acq;
}

void *tardis_mathematics_thread(void *internal_data)
{
    sem_t *acq_sem_in;
    sem_t *acq_sem_out;
    sem_t *offset_sem;
    double *offset;
    double new_offset;
    double working_time;

    uint32_t acq_buff_index;
    struct tardis_time_acq_t *acq_buffer;
    struct tardis_time_acq_t time_acq;
    acq_buffer = ((struct tardis_internal_data *) internal_data)->acq_buffer;
    acq_sem_in = ((struct tardis_internal_data *) internal_data)->acq_sem_in;
    acq_sem_out = ((struct tardis_internal_data *) internal_data)->acq_sem_out;
    offset_sem = ((struct tardis_internal_data *) internal_data)->offset_sem;
    offset = ((struct tardis_internal_data *) internal_data)->offset;

    acq_buff_index = 0;

    struct tardis_time_acq_list *trans_list;
    struct tardis_time_acq_list *trans_cursor;
    struct tardis_time_acq_list *trans_tmp;
    trans_list = NULL;
    trans_tmp = NULL;

    struct tardis_host_time_list *host_list;
    struct tardis_host_time_list *host_cursor;
    struct tardis_host_time_list *host_tmp;
    host_list = NULL;
    host_tmp = NULL;


    bool inserted;
    struct timespec sleep_ts;

    int ret;

    while (1)
    {
        clock_gettime(CLOCK_REALTIME, &sleep_ts);
        if (sleep_ts.tv_nsec > 500000000)
        {
            sleep_ts.tv_sec++;
            sleep_ts.tv_nsec -= 500000000;
        }
        else
        {
            sleep_ts.tv_nsec += 500000000;
        }

        ret = sem_timedwait(acq_sem_out, &sleep_ts);

        if (!ret)
        {
            time_acq = acq_buffer[acq_buff_index];
            acq_buffer[acq_buff_index].cap_ts = 0.0;
            sem_post(acq_sem_in);

            printf("MATHEMATICS: Dequeued %2d\n", acq_buff_index);

            acq_buff_index = (1 + acq_buff_index) % TARDIS_ACQ_BUFF_SIZE;
            inserted = false;
            working_time = time_acq.cap_ts;
        }
        else
        {
            working_time += 0.5;
            inserted = true;
        }

        trans_cursor = trans_list;
        if (!inserted && trans_list == NULL)
        {
            printf("Allocating (null): %f\n", time_acq.trans_ts);
            trans_list = malloc(sizeof(struct tardis_time_acq_list));
            trans_list->next = NULL;
            trans_list->prev = NULL;
            trans_list->val = time_acq;
            inserted = true;
        }
        else if (!inserted)
        {
            while (trans_cursor != NULL)
            {
                printf("Trans Loop: %f\n", trans_cursor->val.trans_ts);
                if (trans_cursor->val.orig_ts >= 0.0)
                    printf("            %f\n", trans_cursor->val.orig_ts);
                if (trans_cursor->val.trans_ts == time_acq.orig_ts
                    && trans_cursor->val.src_ip.s_addr == time_acq.dst_ip.s_addr
                    && trans_cursor->val.src_port == time_acq.dst_port)
                {
                    // Update hostList
                    if (host_list == NULL)
                    {
                        printf("Adding host (null): %d\n", time_acq.src_ip);
                        host_list = malloc(sizeof(struct tardis_host_time_list));
                        host_list->next = NULL;
                        host_list->prev = NULL;
                        host_list->val.host = time_acq.src_ip; // Time lock is on distant end
                        host_list->val.stratum = time_acq.stratum;
                        host_list->val.last_polled = time_acq.cap_ts;
                        host_list->val.poll_interval = TARDIS_DEFAULT_POLL_INTERVAL; //Fix later
                        // Due hard math here
                        host_list->val.offset =
                            ((time_acq.recv_ts - trans_cursor->val.cap_ts)
                                + (time_acq.trans_ts - time_acq.cap_ts)
                            ) / 2.0;
                    }
                    else
                    {
                        host_cursor = host_list;
                        printf("HOST %d = %f\n",
                               host_cursor->val.host,
                               host_cursor->val.offset);

                        while (host_cursor->next != NULL)
                        {
                            if (host_cursor->val.host.s_addr == time_acq.src_ip.s_addr)
                                break;
                            host_cursor = host_cursor->next;
                        }
                        if (host_cursor->val.host.s_addr
                            == time_acq.src_ip.s_addr)
                        {
                            printf("Update host (hit):  %d\n", time_acq.src_ip);
                            host_cursor->val.stratum = time_acq.stratum;
                            host_cursor->val.last_polled = time_acq.orig_ts;
                            host_cursor->val.poll_interval = TARDIS_DEFAULT_POLL_INTERVAL;

                            //Due hard math
                            host_list->val.offset =
                                ((((time_acq.recv_ts - trans_cursor->val.cap_ts)
                                    + (time_acq.trans_ts - time_acq.cap_ts)
                                ) / 2.0) + host_cursor->val.offset) / 2.0;
                        }
                        else
                        {
                            printf("Adding host (miss): %d\n", time_acq.src_ip);
                            host_cursor->next = malloc(sizeof(struct tardis_host_time_list));
                            host_cursor->next->prev = host_cursor;
                            host_cursor = host_cursor->next;
                            host_cursor->next = NULL;
                            host_cursor->val.host = time_acq.src_ip; // Time lock is on distant end
                            host_cursor->val.stratum = time_acq.stratum;
                            host_cursor->val.last_polled = time_acq.orig_ts;
                            host_cursor->val.poll_interval = TARDIS_DEFAULT_POLL_INTERVAL; //Fix later
                            // Due hard math here
                            host_list->val.offset =
                                ((time_acq.recv_ts - trans_cursor->val.cap_ts)
                                    + (time_acq.trans_ts - time_acq.cap_ts)
                                ) / 2.0;
                        }
                    }
                }



                // TODO: Refactor this section into a clean insert function
                if (trans_cursor->val.trans_ts == time_acq.trans_ts
                    && trans_cursor->val.src_ip.s_addr == time_acq.src_ip.s_addr
                    && trans_cursor->val.dst_ip.s_addr == time_acq.dst_ip.s_addr
                    && trans_cursor->val.src_port == time_acq.src_port
                    && trans_cursor->val.dst_port == time_acq.dst_port)
                {
                    inserted = true;
                }
                else if (!inserted && trans_cursor->val.trans_ts > time_acq.trans_ts)
                {
                    trans_tmp = trans_cursor;

                    printf("Allocating (miss): %f\n", time_acq.trans_ts);
                    trans_cursor = malloc(sizeof(struct tardis_time_acq_list));
                    trans_cursor->prev = trans_tmp->prev;
                    trans_cursor->next = trans_tmp;
                    trans_cursor->val = time_acq;

                    trans_tmp->prev = trans_cursor;

                    if (trans_cursor->prev == NULL)
                        trans_list = trans_cursor;
                    else
                        trans_cursor->prev->next = trans_cursor;

                    inserted = true;
                }
                else if (!inserted && trans_cursor->next == NULL)
                {
                    printf("Allocating (end):  %f\n", time_acq.trans_ts);
                    trans_cursor->next = malloc(sizeof(struct tardis_time_acq_list));
                    trans_cursor->next->prev = trans_cursor;
                    trans_cursor->next->next = NULL;
                    trans_cursor->next->val = time_acq;

                    inserted = true;
                }
                // END TODO

                // TODO: Create a clean trimming function
                if (working_time - trans_cursor->val.cap_ts > 5.0)
                {
                    if (trans_cursor->next != NULL)
                        trans_cursor->next->prev = trans_cursor->prev;

                    if (trans_cursor->prev != NULL)
                        trans_cursor->prev->next = trans_cursor->next;
                    else
                        trans_list = trans_cursor->next;

                    trans_tmp = trans_cursor;
                    trans_cursor = trans_cursor->next;
                    printf("Freeing: %f... ", trans_tmp->val.trans_ts);
                    free(trans_tmp);
                    trans_tmp = NULL;
                    puts("Done\n");
                }
                else
                {
                    trans_cursor = trans_cursor->next;
                }
                // END TODO
            }
        }

        new_offset = 0.0;
        host_cursor = host_list;
        double indiv_weight;
        double total_weight;

        total_weight = 0.0;

        while (host_cursor != NULL)
        {
            printf("Reading host: %d\n", host_cursor->val.host);
            indiv_weight = 1.0/(pow(2.0, host_cursor->val.stratum));

            if (working_time - host_cursor->val.last_polled
                > (4 * host_cursor->val.poll_interval))
            { // Delete
                if (host_cursor->prev != NULL)
                    host_cursor->prev->next = host_cursor->next;
                else
                    host_list = host_cursor->next;

                if (host_cursor->next != NULL)
                    host_cursor->next->prev = host_cursor->prev;

                host_tmp = host_cursor;
                host_cursor = host_cursor->next;
                printf("Freeing host: %d...", host_tmp->val.host);
                free(host_tmp);
                printf("Done.");
                continue;
            }
            else if (working_time - host_cursor->val.last_polled
                > (3 * host_cursor->val.poll_interval))
            { // ignore
                host_cursor = host_cursor->next;
                continue;
            }
            else if (working_time - host_cursor->val.last_polled
                > host_cursor->val.poll_interval)
            {
                indiv_weight *= (((working_time-host_cursor->val.last_polled)
                    / host_cursor->val.poll_interval) - 1) / 2.0;
            }

            total_weight += indiv_weight;
            new_offset += indiv_weight * host_cursor->val.offset;
            host_cursor = host_cursor->next;
        }

        if (total_weight > 0.0)
            new_offset /= total_weight;

        sem_wait(offset_sem);
        *offset = new_offset;
        sem_post(offset_sem);
    }
}

void *tardis_redistribution_thread(void *internal_data)
{
    sem_t *offset_sem;
    double *offset;
    double next_offset;
    double curr_offset;
    struct timespec sleep_ts;
    struct timespec next_ts;
    char time_out[11];
    int ret;

    int refclock;

    char str[TARDIS_MAX_FILE_LENGTH];;

    char dumbclock[TARDIS_MAX_FILE_LENGTH];
    snprintf(&dumbclock,
        TARDIS_MAX_FILE_LENGTH,
        "/dev/dumbclock%d",
        ((struct tardis_internal_data *) internal_data)->refclock_val);

    unlink(&dumbclock);
    refclock = getpt();
    if (grantpt(refclock))
        puts("ERROR: grantpt");
    if (unlockpt(refclock))
        puts("ERROR: unlockpt");
    ptsname_r(refclock, &str, TARDIS_MAX_FILE_LENGTH);
    if (chmod(&str, 0644))
        puts("ERROR: chmod");
    if (symlink(&str, &dumbclock))
        puts("ERROR: symlink");



    offset_sem = ((struct tardis_internal_data *) internal_data)->offset_sem;
    offset = ((struct tardis_internal_data *) internal_data)->offset;

    clock_gettime(CLOCK_REALTIME, &next_ts);
    next_ts.tv_sec++;
    next_ts.tv_nsec = 0;
    curr_offset = 0.0;

    while (1)
    {
        sem_wait(offset_sem);
	    printf("Offset: %f\n", *offset);
        next_offset = *offset;
        sem_post(offset_sem);

        strftime(&time_out, 16, "%T", localtime(&next_ts));
        next_ts.tv_sec++;
        time_out[8]  = '\r';
        time_out[9]  = '\n';
        time_out[10] = 0;

        if (next_offset - curr_offset > 0.001)
            next_offset = curr_offset + 0.001;
        else if (next_offset - curr_offset < -0.001)
            next_offset = curr_offset - 0.001;

        if (next_offset > 2.5)
            next_offset = 2.5
        else if (next_offset < -2.5)
            next_offset = -2.5
        curr_offset = next_offset;

        if (next_offset >= 0)
            sleep_ts.tv_sec = next_ts.tv_sec - (long) floor(next_offset);
        else
            sleep_ts.tv_sec = next_ts.tv_sec - (long) ceil(next_offset);

        sleep_ts.tv_nsec = next_ts.tv_sec - ((long) round(next_offset * 1000000000) % 1000000000);
        if (sleep_ts.tv_nsec < 0)
        {
            sleep_ts.tv_sec--;
            sleep_ts.tv_nsec += 1000000000;
        }
        else if (sleep_ts.tv_nsec >= 1000000000)
        {
            sleep_ts.tv_sec++;
            sleep_ts.tv_nsec -= 1000000000;
        }

	    //sleep_ts = next_ts;

        ret = clock_nanosleep(CLOCK_REALTIME, TIMER_ABSTIME, &sleep_ts, NULL);
        if (ret == EFAULT)
            puts("ERROR: EFAULT\n");
        else if (ret == EINTR)
            puts("ERROR: EINTR\n");
        else if (ret == EINVAL)
            printf("ERROR: EINVAL\n%f\n%d.%09d:%d.%09d\n", next_offset, next_ts.tv_sec, next_ts.tv_nsec, sleep_ts.tv_sec, sleep_ts.tv_nsec);
        else if (ret)
            puts("ERROR: Unknown\n");

        write(refclock, &time_out, 11);
    }
    return NULL;
}
