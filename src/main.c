#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "config.h"
#include "tardis.h"

char const *USAGE = (
    "usage: [-h] [-V] -i <iface> (-m <memory_num> | -s <dclock_num>)\n"
    "    -h    print this help menu\n"
    "    -i    interface from which to sniff NTP traffic\n"
    "    -m    shared memory value (see Type 28, NTP refclock)\n"
    "    -s    number identifier for the 'dumb' clock (see Type 33, NTP refclock)\n"
    "    -V    print version information\n");

char const *ERR_MEMORY_NUM_BOUNDS = (
    "ERROR: 'memory_num' must be a number between 0 and 4, inclusive\n");

char const *ERR_DUMBCLOCK_NUM_BOUNDS = (
    "ERROR: 'dclock_num' must be a number between 0 and 9, inclusive\n");

char const *ERR_MANDATORY_ARGS = (
    "ERROR: 'iface' and either 'memory_num' or 'dclock_num' are mandataory arguments\n");

char const *ERR_REFCLOCKS = (
    "ERROR: Only one reference clock implementation is allowed at a time\n"
    "    (i.e. '-s' and '-m' cannot be used together)\n");

int main(int argc, char **argv)
{
    char* iface = "";
    char* endptr = "";
    long shared_mem_val = -1;
    long dumb_clock_val = -1;

    char c;
    while ( (c = getopt(argc, argv, "hi:m:s:V")) != -1)
    {
        switch (c)
        {
            case 'i':
                iface = optarg;
                break;

            case 'm':
                errno = 0;
                shared_mem_val = strtol(optarg, &endptr, 10);
                if (errno != 0
                    || endptr == optarg
                    || endptr - optarg != strlen(optarg)
                    || shared_mem_val < 0
                    || shared_mem_val > 4)
                {
                    fputs(ERR_MEMORY_NUM_BOUNDS, stderr);
                    exit(1); // Arg out of bounds (error: explain)
                }
                break;

            case 's':
                errno = 0;
                dumb_clock_val = strtol(optarg, &endptr, 10);
                if (errno != 0
                    || endptr == optarg
                    || endptr - optarg != strlen(optarg)
                    || dumb_clock_val < 0
                    || dumb_clock_val > 9)
                {
                    fputs(ERR_DUMBCLOCK_NUM_BOUNDS, stderr);
                    exit(1); // Arg out of bounds (error: explain)
                }
                break;

            case 'V':
                printf("%s\n", PACKAGE_STRING);
                exit(0); // Print version string (no error)

            case 'h':
                puts(USAGE);
                exit(0); // Print usage (no error)

            default:
                fputs(USAGE, stderr);
                exit(-1); // Invalid option (error: print usage)
            }
        }

    if (strlen(iface) == 0 || (shared_mem_val == -1 && dumb_clock_val == -1))
    {
        fputs(ERR_MANDATORY_ARGS, stderr);
        fputs(USAGE, stderr);
        exit(1); // Invalid invokation
    }

    if (shared_mem_val != -1 && dumb_clock_val != -1)
    {
        fputs(ERR_REFCLOCKS, stderr);
        fputs(USAGE, stderr);
        exit(1);
    }

    exit(tardis_launch(iface, dumb_clock_val));
}
