#include "config.h"

#include <arpa/inet.h>

#define TARDIS_PCAP_SNAPLEN 65535
#define TARDIS_PCAP_TIMEOUT 5
#define TARDIS_PCAP_PROMISC 1
#define TARDIS_BPF_FILTER "udp and port 123"
#define TARDIS_BPF_OPTIMIZE 1

#define TARDIS_NTP_UNIX_OFFSET 2208988800L
#define TARDIS_NTP_FRACT_SECS 0.000000000232830644
#define TARDIS_ACQ_BUFF_SIZE 100
#define TARDIS_DEFAULT_POLL_INTERVAL 1024.0

#define TARDIS_MAX_FILE_LENGTH 30

struct tardis_ntp_t {
    uint8_t flags;
    uint8_t stratum;
    uint8_t poll;
    uint8_t precision;
    uint32_t root_delay;
    uint32_t root_dispersion;
    struct in_addr referenceID;
    uint32_t ref_ts_sec;
    uint32_t ref_ts_frac;
    uint32_t origin_ts_sec;
    uint32_t origin_ts_frac;
    uint32_t recv_ts_sec;
    uint32_t recv_ts_frac;
    uint32_t trans_ts_sec;
    uint32_t trans_ts_frac;
};

struct tardis_time_acq_t {
    double cap_ts;
    struct in_addr src_ip;
    struct in_addr dst_ip;
    uint16_t src_port;
    uint16_t dst_port;
    uint8_t stratum;
    struct in_addr ref_id;
    double ref_ts;
    double orig_ts;
    double recv_ts;
    double trans_ts;
};

int tardis_launch(char* interface, int shm_val);
