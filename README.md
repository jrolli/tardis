# Time Acquisition & Re-DIStribution (TARDIS)
_Protecting network defenders from the dangers of the Time Vortex._

## Overview
TARDIS passively extracts network time information (i.e. NTP exchanges) utilizing `libpcap` and shares them with an NTP server by acting as 'shared memory' (SHM) reference clock (REFCLOCK).  This enables isolated monitoring networks to synchronize their internal time with the target network's observed time which in turn allows better correlation of data.  Support first focuses on [NTPv3 (RFC 1305)](http://www.ietf.org/rfc/rfc1305.txt) and then [NTPv4 (5905)](http://www.ietf.org/rfc/rfc5905.txt).

## Concept
The basic principle of TARDIS is fairly simple, pretend to be the originator of an NTP query and substitue the relevant time fields when doing the time difference calculations.  The basic idea behind NTP is to send local time to a remote time server and then calculate clock difference by attempting to account for network latency.  Specifically, the requestor sends `t0` (time request sent) and the time server responds with `t0`, `t1` (time request received), and `t2` (time response sent).  Upon receiving the response, the requestor records `t3` (time response received) and calculates `((t1-t0) + (t2-t3))/2`.  By tracking requests and the associated responses, TARDIS substitutes its local receipt times of the request and response for `t0` and `t3`, respectively.  While not a perfect solution, this provides a good reference for synchronizing an isolated monitoring network to the target network, regardless of the actual upstream source of time.

## Design
TARDIS utilizes two threads to synchronize time with `ntpd`.  The first thread is the acquisition thread which handles the reading of NTP packets and updating of the current "model" for time (i.e. adjustment from local system time); this thread is controlled by `pcap_loop`.  The second thread answers time queries from `ntpd` that are directed through shared memory and is controlled by `libev`.  Concurrency is managed by ensuring `libev` can only read TARDIS time and `pcap_loop` does not release references to the model immediately to prevent use-after-free errors.

### Time Acquisition Thread
#### Target Time Acquisition
TARDIS acquires time from the target network by using `libpcap` on a promiscuous interface to collect in-transit NTP messages.  By limiting the collection to 123/UDP -> 123/UDP and packets with the correct data length, TARDIS is computationally efficient enough to run on a CPU-bound sensor without significantly degrading the performance of other processes (e.g. Suricata, Bro, YAF, etc.).

#### Time Modelling
TARDIS will likely be estimating its time difference among dozens, if not hundreds, of hosts simultaneously.  Because of this, it prioritizes two things:  (1) minimizing the aggregate time difference with "local" hosts (as defined in its configuration) and (2) ensuring that the time it presents is monotonically increasing (time never flows backwards).  While it does not need to meet the strict requirements of `ntpd`'s time guarantees, these two traits are important for minimizing quirks on the monitoring network and ensuring the best time synchronization with the target network. 

### Time Re-Distribution Thread
#### Shared-Memory Reference Clock Driver
**NOT IMPLEMENTED**: TARDIS can utilize the SHM REFCLOCK driver (Type 28) to pass time to a locally running NTP server.  This allows TARDIS to run as a separate process while minimizing overhead and complexity in the IPC with `ntpd`.

#### Dumb Reference Clock Driver
TARDIS prints the time every second to a pseudo-terminal in the format that the NTP 'dumb' reference clock driver (Type 33) expects.  While the time-lock may not be as accurate as the shared-memory driver, the overall architecture is simpler and easier to interconnect with other programs.

##### Usage
In `ntp.conf` ensure that you have the following line:
```
server 127.127.33.0
```

Then start TARDIS:
```
tardis -i eth0 -s 0
```

Finally, restart NTP.

Notes:
  - Ensure that the last octet in the `ntp.conf` and the parameter passed to TARDIS' `-s` flag match.
  - If running on RHEL or CentOS, ensure you invoke `setsebool daemons_use_tty=1` prior to starting up TARDIS.

## Architecture
### Startup
#### Read config
#### Spawn threads

### Running
#### Ingest NTP packets
#### Respond to time requests
#### Logging

### Teardown
#### Restore interface
#### Handle shared memory
#### Send proper exit status
